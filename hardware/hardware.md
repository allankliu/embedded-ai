## Hardware

### MCU

- RP2040 from Raspberry Foundations
- STM32H7 from STM
- GD32H7 from Giga Devices

### SoC

- ESP32 from Espressif

### MPU

- K210 from Kendryte
- K510 from Kendryte
- RK1808 from Rockchip
- RK1103/RK1106 from Rockchip
- D1 from Allwinner
- CV1800B
- BL808 from Bofflo labs

### SBC

- LuckFox-pico with RK1103/1106
- MilkV-duo with CV1800B
- M1sDock with BL808

### FPGA

Not started yet.
