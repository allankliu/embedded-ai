# embedded-ai

## About

The project is dedicated to collect all kinds of information for Embedded AI/ML, including hardware/software/model/papers.

The Embedded AI has many differences from its counterpart in cloud in execution enviornment. 

I will collect Embedded AI in the MCU/FPGA/MPU/SoC/SBC and mobile/edge platforms.
## Folders

- Books and Papers, including all kinds of basic concept behind AI/ML/DL/NN.
- Chips and Boards, including hardware related information to run.
- Models, of TensorFlow, TFL, TinyML, PyTorch, ONNX. 
- Applications, all related products and information.
